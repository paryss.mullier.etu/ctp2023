
public class Personne {

    private String nom;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        if (nom == null) {
            throw new IllegalArgumentException("le nom ne peut pas être null");
        }
        this.nom = nom;
    }

    public boolean equals(Object o) {
        if (o instanceof Personne) {
            Personne personne = (Personne) o;
            return nom.equals(personne.nom);
        }
        return false;
    }

}
