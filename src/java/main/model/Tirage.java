
public class Tirage {

    private ArrayList<Inscrit> inscrits;

    public Tirage() {
        inscrits = new ArrayList<Inscrit>();
    }

    public void ajouter(Personne p) {
        inscrits.add(p);
    }

    public Personne get(int i) {
        return inscrits.get(i);
    }

    public void retirer(Personne p) {
        inscrits.remove(p);
    }

    public int size() {
        return inscrits.size();
    }

    public void melange() {
        inscrits.shuffle();
    }

    public void tirage() {
        for (int i = 1; i < inscrits.size(); i++) {
            inscrits.get(i).donneA(inscrits.get(i - 1));
            inscrits.get(i - 1).receveurDe(inscrits.get(i));
        }
    }

    public void trier() {
        inscrits.sort();
    }
}
