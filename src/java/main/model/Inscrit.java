public class Inscrit {

    private Personne personne;
    private Personne donneur;
    private Personne receveur;

    public Inscrit(Personne personne) {
        this.personne = personne;
    }

    public void receveurDe(Personne personne) {
        this.donneur = personne;
    }

    public Personne getDonneur() {
        return donneur;
    }

    public void donneA(Personne personne) {
        this.receveur = personne;
    }

    public Personne getReceveur() {
        return receveur;
    }

    public boolean equals(Object o) {
        if (o instanceof Inscrit) {
            Inscrit inscrit = (Inscrit) o;
            return personne.equals(inscrit.personne);
        }
        return false;
    }
}
