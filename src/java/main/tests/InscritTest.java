public class InscritTest {

    @org.junit.Test
    public void testDonneur() {
        Personne personne1 = new Personne();
        personne1.setNom("Dupont");
        Personne personne2 = new Personne();
        personne2.setNom("Durand");
        Inscrit inscrit = new Inscrit(personne1);
        inscrit.receveurDe(personne2);
        org.junit.Assert.assertEquals(personne2, personne1.getDonneur());
    }

    @org.junit.Test
    public void testReceveur() {
        Personne personne1 = new Personne();
        personne1.setNom("Dupont");
        Personne personne2 = new Personne();
        personne2.setNom("Durand");
        Inscrit inscrit = new Inscrit(personne1);
        inscrit.donneA(personne2);
        org.junit.Assert.assertEquals(personne2, personne1.getReceveur());
    }
}
