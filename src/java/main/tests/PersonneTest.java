
public class PersonneTest {
    @org.junit.Test
    public void testNom() {
        Personne personne = new Personne();
        personne.setNom("Dupont");
        org.junit.Assert.assertEquals("Dupont", personne.getNom());
        personne.setNom("");
        org.junit.Assert.assertEquals("", personne.getNom());
    }
}