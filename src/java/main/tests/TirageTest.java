public class TirageTest {

    @org.junit.Test
    public void testAjouter() {
        Personne personne = new Personne();
        personne.setNom("Dupont");
        Tirage t = new Tirage();
        t.ajouter(personne);
        org.junit.Assert.assertEquals(personne, t.get(1));
    }

    @org.junit.Test
    public void testRetirer() {
        Personne personne = new Personne();
        personne.setNom("Dupont");
        Tirage t = new Tirage();
        t.ajouter(personne);
        t.retirer(personne);
        org.junit.Assert.assertEquals(0, t.size());
    }
}
